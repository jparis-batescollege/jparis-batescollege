
# :fire: Stuff

* [TODOs](https://gitlab.com/dashboard/todos)
* [My **Filtered** Issues](https://gitlab.com/dashboard/issues?sort=updated_desc&state=opened&assignee_username%5B%5D=jparis-batescollege&not%5Blabel_name%5D%5B%5D=status::needs+discussion&not%5Blabel_name%5D%5B%5D=status::needs+review&not%5Blabel_name%5D%5B%5D=status::in+review&not%5Blabel_name%5D%5B%5D=status::needs+deploy&not%5Blabel_name%5D%5B%5D=Blocked&not%5Blabel_name%5D%5B%5D=status::blocked&not%5Blabel_name%5D%5B%5D=status::on+hold&not%5Blabel_name%5D%5B%5D=status::awaiting+client+sign-off&not%5Blabel_name%5D%5B%5D=Ready+to+Merge)
* [All of my Issues](https://gitlab.com/dashboard/issues?sort=updated_desc&state=opened&assignee_username[]=jparis-batescollege)
* [:star: issues](https://gitlab.com/dashboard/issues?sort=updated_desc&state=opened&my_reaction_emoji=Any)
* [Code search in WordPress project](https://gitlab.com/search?group_id=68610166&scope=blobs)
* [:star: MRs](https://gitlab.com/dashboard/merge_requests?scope=all&state=opened&my_reaction_emoji=Any)
* [Labels Reference](https://gitlab.com/bates-ils/projects/gitlab/bates-processes/-/issues/9) | [_My_ labels reference](https://jake.catapult.bates.edu/reference/gitlab-label-colors.php)
* [WebTech Issue Board](https://gitlab.com/bates-ils/teams/sdi/webtech/webcheck/-/boards)
* [My SDI Inbox](https://gitlab.com/bates-ils/people/jparis/meta/-/issues)

# Projects Shortcuts

* [Bates-ILS Projects](https://gitlab.com/bates-ils/projects/)
* [WordPress](https://gitlab.com/bates-ils/projects/wordpress/) -- ([Meta issues](https://gitlab.com/bates-ils/projects/wordpress/meta/-/issues))
* Banner Webdocs: [b9](https://gitlab.com/bates-ils/projects/banner/webdocs/b9), [b8](https://gitlab.com/bates-ils/projects/banner/webdocs/b8), [_Meta](https://gitlab.com/bates-ils/projects/banner/webdocs/meta)